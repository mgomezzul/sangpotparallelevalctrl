/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "ctrlsysData.h"
#include "execParserCtrlSys.h"

/*Crea una copia de un string ingresado en un arreglo*/
char*
newCopyString(char n[]){
  char *p;
  int longitud;

  longitud = strlen(n);
  p = (char *) malloc(longitud + 1);
  strcpy(p,n);
  return p;
}

/*Elimina un evaluador*/
void
deleteStructEvaluador(pevaluador_t ev) {
  free(ev->id);
  free(ev->ruta);
  free(ev->nombreArchivo);
  free(ev);
  return;
}
/*Crea un evaluador*/
pevaluador_t
createEvaluador(char id[], char ruta[], char nombreArchivo[]) {
  pevaluador_t ev;

  ev = (pevaluador_t) malloc(sizeof(evaluador_t));
  ev->id = id;
  ev->ruta = ruta;
  ev->nombreArchivo = nombreArchivo;
  
  return ev;
}
/*Crea una nueva lista de evaluadores*/
plEvaluadores_t
newLEvaluadores(void) {
  plEvaluadores_t le = (plEvaluadores_t) malloc(sizeof(lEvaluadores_t));
  le->ev = (pevaluador_t)NULL;
  le->next = (plEvaluadores_t)NULL;
  return le;
}
/*Adiciona un nuevo evaluador a la lista*/
plEvaluadores_t
addLista(plEvaluadores_t head, pevaluador_t ev) {
  plEvaluadores_t nHead = newLEvaluadores();
  nHead->ev = ev;
  nHead->next = head;
  return nHead;
}

pevaluador_t
getEvaluador(plEvaluadores_t list, int num) {
  
  plEvaluadores_t current = list;

  if (!list) {
    fprintf(stderr, "ctrlsysData.c: Error: lista de evaluadores nula.\n");
    return (pevaluador_t)NULL;
  } else {
    int i;
    for(i = 0; i < num; ++i) {
      current = current->next;
      if (!current) {
	fprintf(stderr, "ctrlsysData.c: La lista no contiene tantos elementos.\n");
	return (pevaluador_t)NULL;
      }
    }
    if (!current->ev) {
      fprintf(stderr, "ctrlsysData.c: Error: evaluador nulo.\n");
      return (pevaluador_t)NULL;
    }
    return current->ev;
  }
}

/*Retorna el número de evaluadores*/
int getNumEvaluadores(plEvaluadores_t le) {
  
  if (le) {
    if (!le->ev) {
      fprintf(stderr, "ctrlsysData.c: Error: lista de evaluadores corrupta.\n");
      return -1;
    }
    int num = 0;
    do {
      num++;
      le = le->next;
    } while(le);
    return num;
  } else {
    fprintf(stderr, "ctrlsysData.c:Error: lista de evaluadores nula.\n");
    return -1;
  }
}
