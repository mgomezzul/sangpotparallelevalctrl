/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

%{
#include <stdio.h>
#include <stdlib.h>
#include "ctrlsysData.h"

int yylex();
void yyerror(const char *msg);
%}

%union {
  char *vtext;
  struct LEvaluadores* vlista;
  struct Evaluador*  vev;
}

%token EVALUADOR ABRELLAVE CIERRALLAVE
%token <vtext>   ID PATH EVALCFG
%start abssyn
%type <vlista> abssyn
%type <vlista> archcfg
%type <vev> evaluador
%%

abssyn : 	archcfg  { yylval.vlista = $1; }
		;

archcfg: 	evaluador archcfg { $$ = addLista($2, $1); }
|	evaluador { $$ = addLista((struct LEvaluadores*)NULL, $1); }	
		;

evaluador: 	EVALUADOR ID ABRELLAVE PATH EVALCFG CIERRALLAVE 
		{ printf("PATH:%s\nFILE:%s\n", $4, $5); $$ = createEvaluador($2, $4, $5); }
		;
%%
