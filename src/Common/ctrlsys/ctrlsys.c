/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "ctrlsysData.h"
#include "execParserCtrlSys.h"

#define MAX_ENTORNO_SZ 1000
#define MAX_PATH_SZ 300
#define MAX_EVALCFG_SZ 300
#define MAX_PROCESS_SZ 100

static const char DIRDETRABAJO[] = "DIRDETRABAJO=";
static const char FICHEROCFG[] = "FICHEROCFG=";

/* Si se presenta un error, devuelve una explicacion de como se usa el programa */
void
usage(char *progname) {
  fprintf(stderr, "Usage %s\n", progname);
  exit(1);
}


int crearProcesoCtrlEval(int index, char const *envp[])
{
  return 0; 
}


void
initPipe(int **matrix, int size)
{
  int i;
  for (i = 0; i < size; ++i) {
    matrix[i] = (int*)malloc(2*sizeof(int));
  }
}


int 
main(int argc, char *argv[]) {

  if (argc != 1) {
    usage(argv[0]);
  }

  FILE *inFile   = fopen("ctrlsys.cfg", "r");

  if (!inFile) {
    usage(argv[0]);
  }

  plEvaluadores_t lEvaluadores = NULL;

  if ((lEvaluadores = execParserCtrlSys(inFile))) {
    
    char **envp = (char**)malloc(3*sizeof(char*));
    envp[0] = (char*)malloc(MAX_PATH_SZ*sizeof(char));
    envp[1] = (char*)malloc(MAX_PATH_SZ*sizeof(char));
    envp[2] = NULL;
    memset(envp[0], 0, MAX_PATH_SZ);
    memset(envp[1], 0, MAX_PATH_SZ);
    fprintf(stdout, "¡Parser archivo de configuración funcionó!\n");
    char *entorno = (char*)malloc(MAX_ENTORNO_SZ*sizeof(char));
    int num_evaluadores = getNumEvaluadores(lEvaluadores);
    int i;
    //int pipe_in[MAX_PROCESS_SZ][2];
    //int pipe_out[MAX_PROCESS_SZ][2];
    int **pipe_in = (int**)malloc(num_evaluadores*sizeof(int*));
    int **pipe_out = (int**)malloc(num_evaluadores*sizeof(int*));
    initPipe(pipe_in, num_evaluadores);
    initPipe(pipe_out, num_evaluadores);
    pid_t *ptChild = (int*)malloc(num_evaluadores*sizeof(int));    
    for (i = 0; i < num_evaluadores; ++i) {
      strcpy(envp[0], DIRDETRABAJO);
      strcpy(envp[1], FICHEROCFG);
      
      if (pipe(pipe_in[i]) == -1) { 
	
	/* Algo salió mal */
	fprintf(stderr, "Error: Debido a: %d %s, en pipe[%d]\n", errno, strerror(errno), i);
      }
      
      if (pipe(pipe_out[i]) == -1) {
	fprintf(stderr, "Error: Debido a: %d %s, en pipe[%d]\n", errno, strerror(errno), i);
      }
      
      pevaluador_t ev = getEvaluador(lEvaluadores, i);
      strcat(envp[0], ev->ruta);
      strcat(envp[1], ev->nombreArchivo);
      /* Crea el hijo */
      if ((ptChild[i] = fork()) == 0) {
	
	/*close unneeded */
	close(pipe_in[i][1]);
	close(pipe_out[i][0]);
	
	/* Make pipes the stdin and stdout */
	close(0);
	dup2(pipe_in[i][0], 0);
	close(pipe_in[i][0]); 
	
	close(1);
	dup2(pipe_out[i][1], 1);
	close(pipe_out[i][1]);
	
	/*run the child process */
	
	execle("../ctrleval/ctrleval", "ctrleval", (char *) NULL, envp);
	/* Algo malo ocurrió */
	fprintf(stderr, "Error: este proceso nunca debería alcanzar este punto\n");
	exit(2);
      } else /* Parent */ {
	
	memset(envp[0], 0, MAX_PATH_SZ);
	memset(envp[1], 0, MAX_PATH_SZ);
	/* close unneeded */
	close(pipe_in[i][0]);
	close(pipe_out[i][1]);
	
      }
    }
    memset(entorno, 0, MAX_ENTORNO_SZ);
    strcat(entorno, "{}");
    while (1) {
      for(i = 0; i < num_evaluadores; ++i) {
	size_t tam;
	write(pipe_in[i][1], entorno, strlen(entorno));
	memset(entorno, 0, MAX_ENTORNO_SZ);
	if ((tam = read(pipe_out[i][0], entorno, MAX_ENTORNO_SZ)) >= 0) {
	  
	  entorno[tam] = '\0';
	  // close(pipe_out[i][0]);
	  //buf[tam] = '\0';
	  fprintf(stderr, "ctrlsys.c: entorno == '%s'\n", entorno);
	  //sleep(3);
	} else { 
	  fprintf(stderr, "ctrlsys.c: error = %d \n", errno);
	  exit(1);
	}
      }
    }
    
  } else {
    
    fprintf(stderr, "¡Parser archivo de configuración no funcionó!\n");
  }
  return 0;
  
}
