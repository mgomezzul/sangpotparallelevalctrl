/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#ifndef CTRLSYSDATA_H
#define CTRLSYSDATA_H

/*Esta estructura corresponde a Evaluador que contiene 3 cadenas de caracteres identificando un id, ruta y nombre del archivo*/
struct Evaluador {
  char *id;
  char *ruta;
  char *nombreArchivo;
};

/*Crea una copia de un string ingresado en un arreglo*/
char * newCopyString(char[]);

/*Definen los tipos de estructuras, que evitan el tener que volver a declarar tipos de variables*/
typedef struct Evaluador   evaluador_t;
typedef struct Evaluador*  pevaluador_t;
typedef struct LEvaluadores  lEvaluadores_t;
typedef struct LEvaluadores* plEvaluadores_t;

/*Esta estructura corresponde a LEvaluadores que contiene un apuntador a evaluador y un apuntador a LEvaluadores*/
struct LEvaluadores {
  pevaluador_t  ev;
  plEvaluadores_t next;
};

/*Función que elimina evaluadores*/
void deleteStructEvaluador(pevaluador_t ev);
/*Función que crea un evaluador*/
pevaluador_t createEvaluador(char id[], char ruta[],
				       char nombreArchivo[]);
/*Función que crea una nueva lista de evaluadores*/
plEvaluadores_t newLEvaluadores(void);
/*Función que adiciona un nuevo evaluador a la lista*/
plEvaluadores_t addLista(plEvaluadores_t head, pevaluador_t ev);

/* 
 * Retorna un apuntador al evaluador con el índice requerido en
 * la lista de evaluadores ingresada.
 */
pevaluador_t getEvaluador(plEvaluadores_t list, int num);

/*Función que retorna el número de evaluadores*/
int getNumEvaluadores(plEvaluadores_t le);

#endif
