/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#include "ctrlsysParser.h"
#include "ctrlsysLexer.h"
#include "ctrlsysData.h"
#include "execParserCtrlSys.h"

extern int yyparse();
/*Ejecuta el parser del CtrlSys al recibir un archivo en el que identifica y retorna los evaluadores*/
plEvaluadores_t
execParserCtrlSys(FILE *inFile) {

    yyin = inFile;
    
    if (!yyparse()) {

	plEvaluadores_t evaluadores = yylval.vlista;
	return evaluadores;

    }

    return (plEvaluadores_t)NULL;
    
}

void yyerror(const char *msg) {
    fprintf(stderr, "%s at %d  in '%s'\n", msg, yylineno, yytext);
}
