/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#ifndef EXECPARSERENTORNO_H
#define EXECPARSERENTORNO_H
#include "entorno.h"

/*Función que ejecuta el parser del entorno al recibir una cadena de caracteres a evaluar*/
pvariablesHash_t execParserEntorno(const char*);

#endif
