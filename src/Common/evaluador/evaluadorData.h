/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#ifndef EVALUADOR_H
#define EVALUADOR_H

#include "entorno.h"

/* 
 * La evaluacion se realiza mediante estructuras de arboles.
 * Definiciones correspondientes para el árbol de operaciones y su evaluación.
 */


/* enum correspondiente a los tipos de operadores manejados (cinco) */
enum operator { O_SUM, O_RES, O_MUL, O_DIV, O_UMIN };


/* Union contiene informacion que se encuentra en un nodo.
 * Un nodo contiene informacion correspondiente a un operador, 
 * un valor numerico o un identificador de variables */

union info {
    enum operator oper;
    int numero;
    char *id;
};

/* Si es operador contenemos en info un elemento de tipo operador, si es un numero un elemento
 * de tipo numero y si es string un elemento de tipo string. */
enum tagInfo { T_NUMBER, T_OPER, T_ID };

struct node {
    enum tagInfo tagInfo;
    union info info;
    struct node *leftChild, *rightChild;
};

typedef struct node Node;
typedef struct node* PNode;

PNode newNode(); 		//Crea un nuevo nodo.
char* newID(char*); 		//Realiza una copia de un string.
PNode newSum(PNode,PNode);	//Crea un nodo cuyo operador es una suma y operandos son dos nodos.
PNode newRes(PNode,PNode); 	//Crea un nodo cuyo operador es una resta y operandos son dos nodos.
PNode newMul(PNode,PNode); 	//Crea un nodo cuyo operador es una multiplicacion y operandos son dos nodos.
PNode newDiv(PNode,PNode); 	//Crea un nodo cuyo operador es una division y operandos son dos nodos.
PNode newUmin(PNode); 		//Recibe un nodo y crea un nodo de negacion unitaria.
PNode newNumber(int); 		//Crea un nuevo nodo con un valor numerico.
PNode newIDN(char*); 		//Crea un nuevo nodo con un valor String.


// Realiza la evaluacion de un arbol
int eval(PNode, pvariablesHash_t);


void deleteAssignationTree(PNode node);
/*
 * Definitions corresponding to the assignations set
 */

/* Realiza las asignaciones de una gramatica (ej. a = 3 + b) */
struct Asignacion {
    char* id;
    PNode assignationTree;
};

typedef struct Asignacion     Asignacion_t;
typedef struct Asignacion*    pAsignacion_t;
typedef struct LAsignaciones  lAsignaciones_t;
typedef struct LAsignaciones* plAsignaciones_t;

/*Lista de asignaciones, todas las asignaciones de la gramatica */
struct LAsignaciones {
    pAsignacion_t pa;
    plAsignaciones_t next;
};

pAsignacion_t createAsignacion(char *id, PNode node); 					//Crea una nueva asignacion
plAsignaciones_t newLAsignaciones(void); 						//Crea una nueva lista de asignaciones
plAsignaciones_t addListaAsignaciones(plAsignaciones_t head, pAsignacion_t pa); 	//Agrega asignaciones a la lista de asignaciones
pvariablesHash_t evalLAsignaciones(plAsignaciones_t list, pvariablesHash_t entorno); 	//Evalua todas las listas de asignaciones

int deleteAsignacion(pAsignacion_t pa);
void deleteLAsignaciones(plAsignaciones_t asignaciones);

/* Manejo de los conjuntos de la gramatica */
struct Conjunto {
    int id;
    plAsignaciones_t asignaciones;
};

typedef struct Conjunto    Conjunto_t;
typedef struct Conjunto*   pConjunto_t;
typedef struct LConjuntos  lConjuntos_t;
typedef struct LConjuntos* plConjuntos_t;

struct LConjuntos {
    pConjunto_t pc;
    plConjuntos_t next;
};

pConjunto_t createConjunto(int id, plAsignaciones_t asignaciones);
plConjuntos_t newLConjuntos(void);
plConjuntos_t addListaConjuntos(plConjuntos_t head, pConjunto_t pc);
void showLConjuntos(plConjuntos_t list, pvariablesHash_t entorno);
pvariablesHash_t evalConjunto(int numero, plConjuntos_t list, pvariablesHash_t entorno);

int deleteConjunto(pConjunto_t pc);
void deleteLConjuntos(plConjuntos_t list);
#endif
