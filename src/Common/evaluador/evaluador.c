/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "evaluadorData.h"
#include "entorno.h"
#include "execParserEvaluador.h"
#include "execParserEntorno.h"

/* Clase principal del metodo evaluador */

/* Maximum entorno size + 1. */
#define MAX_ENTORNO_SZ 1000

/* Si se presenta un error, devuelve una explicacion de como se usa el programa */
void
usage(char *progname) {
    fprintf(stderr, "Usage %s <cfg file> <expr file>\n", progname);
    exit(1);
}

/* Metodo principal, toma un conjunto de argumento que se definio en la gramatica
 * Verifica que el numero del conjunto a evaluar no se exceda del numero total de conjuntos
 * y realiza la evaluacion del conjunto pertinente recibido por la entrada estandar, 
 * actualiza el entorno y escribe por la salida estandar */

int 
main(int argc, char *argv[]) {


  //const char *new_entorno;
  char *entorno;
  if (argc != 3) {
    usage(argv[0]);
  }
  char *ctrleval = argv[1];
  int conjunto = atoi(argv[2]);
  FILE *inFile;

  ssize_t tam;
  entorno = (char*)malloc(MAX_ENTORNO_SZ*sizeof(char));
  memset(entorno, 0, MAX_ENTORNO_SZ);
  inFile = fopen(ctrleval, "r");
  if ((tam = read(0, entorno, 1000)) != (ssize_t)(-1)) {
    //entorno[tam] = '\0';
    if (!inFile) {
      usage(argv[0]);
    }
    entorno[tam] = '\0';
    pvariablesHash_t variables_table;
    fprintf(stderr, "evaluador.c: env: %s\n", entorno);
    
    variables_table = execParserEntorno(entorno);
    
    fprintf(stderr, "¡Parser de entorno funcionó!\n");
    //get_entorno(variables_table);
    
    plConjuntos_t lConjuntos = NULL;
    
    if ((lConjuntos = execParserEvaluador(inFile))) {
      //fclose(inFile);
      /*if (variables_table == NULL) 
	fprintf(stderr,"variables_table == NULL\n");
      else
	fprintf(stderr,"variables_table ain't NULL\n");
      */
      fprintf(stdout, "evaluador.c: ¡Parser evaluador funcionó!\n");
      variables_table = evalConjunto(conjunto, lConjuntos, variables_table);
      get_entorno(variables_table, entorno, MAX_ENTORNO_SZ);
      fprintf(stderr, "evaluador.c: entorno == ");
      write(1, entorno, strlen(entorno));
      fprintf(stderr, " \n");
      delete_all(variables_table);
      int i = 2;
      fprintf(stderr, "evaluador.c: **Acabo de despertar de un maravilloso sueño**!\n");
      
      while(1) {
	
	memset(entorno, 0, MAX_ENTORNO_SZ);
	
	if ((tam = read(0, entorno, MAX_ENTORNO_SZ)) != -1) {
	  
          entorno[tam] = '\0';
	  
	  if ((variables_table = execParserEntorno(entorno))) {
	    
	    fprintf(stderr, "¡Parser de entorno funcionó! en la %d vez.\n", i);
	    variables_table = evalConjunto(conjunto, lConjuntos, variables_table);
	    /* memset is done within get_entorno() */
	    get_entorno(variables_table, entorno, MAX_ENTORNO_SZ);
	    fprintf(stderr, "evaluador.c: entorno == ");
	    write(1, entorno, strlen(entorno));
	    fprintf(stderr, " \n");
	    delete_all(variables_table);
	    fprintf(stderr, "evaluador.c: **Acabo de despertar de un maravilloso sueño**!\n");
	    
	  } else {
	    fprintf(stderr, "¡Parser de entorno no funcionó! en la %d vez.\n", i);
	  }
	} else {
	  fprintf(stderr, "evaluador.c: Error: falló la operación de lectura en while(1). ");
	  fprintf(stderr, "En la %d vez.\n", i);
	}
	++i;
      }
    } else {
      
      fprintf(stderr, "¡Parser evaluador no funcionó!\n");
    }
    
  } else {
    fprintf(stderr, "Error: %s\n",strerror(errno)); 
  }

  return 0;
}
