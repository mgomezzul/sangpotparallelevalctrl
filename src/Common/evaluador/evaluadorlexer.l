%{

/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include "evaluadorparser.h"
#include "evaluadorData.h"
%}


%option noyywrap

%%
[\t \n]*                               {                             }
Conjunto                               { return CONJUNTO;            }
[a-zA-Z]([a-zA-Z0-9]*)                 { yylval.vtext = newID(yytext);
                                         return ID;
                                       }
([1-9]([0-9]*)|0)                      { yylval.vnumber = atoi(yytext);
                                         return NUMBER;
                                       }
\{                                     { return ABRELLAVE;}
\}                                     { return CIERRALLAVE;}
=                                      { return ASIGNACION;}
;                                      { return PUNTOYCOMA;}
\+                                     { return SUMA;}
\-                                     { return RESTA;}
\*                                     { return MULTIPLICACION;}
\/                                     { return DIVISION;}
\(                                     { return ABREPARENTESIS;}
\)                                     { return CIERRAPARENTESIS;}
<<EOF>>                                { yyterminate();              }
%%


