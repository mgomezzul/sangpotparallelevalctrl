%{

/* Santiago Angee Agudelo             Codigo: 201010031010
 * Juan Pablo Osorio Restrepo         Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio     Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M. 
 * Universidad EAFIT  */

#include "entorno.h"
#include "entornoparser.h"
%}

%option noyywrap

%%
[\t \n]* ;
\{                      { return ABRELLAVE;                                          }
\}                      { return CIERRALLAVE;                                        }
=                       { return ASIGNACION;                                         }
;                       { return PUNTOYCOMA;                                         }
[a-zA-Z][a-zA-Z0-9]*    { entornolval.vname = newCopyString(entornotext); return ID; }
(\-)?([1-9][0-9]*|0)    { entornolval.vvalue = atoi(entornotext); return NUMBER;     }
<<EOF>>                 { yyterminate();                                             }
%%
