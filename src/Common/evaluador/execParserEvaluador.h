/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

#ifndef EXECPARSEREVALUADOR_H
#define EXECPARSEREVALUADOR_H
#include "evaluadorData.h"

/*Función que ejecuta el parser del evaluador al recibir un archivo para evaluar*/
plConjuntos_t execParserEvaluador(FILE* inFile);

#endif
