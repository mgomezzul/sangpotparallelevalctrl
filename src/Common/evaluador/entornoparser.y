/* Santiago Angee Agudelo            Codigo: 201010031010
 * Juan Pablo Osorio Restrepo        Codigo: 201010025010
 * Luisa Fernanda Querubin Osorio    Codigo: 200910006010
 * Practica numero uno para Sistemas Operativos 2012-2
 * Profesor: Juan Francisco Cardona M.
 * Universidad EAFIT  */

%{
#include <stdio.h>
#include <stdlib.h>
#include "entorno.h"

int entornolex(void);
void entornoerror(const char *msg);
%}

%union {
    char *vname;
    int vvalue;
    struct variable*   var;
    struct variablesHash* vars;
}

%token ABRELLAVE CIERRALLAVE ASIGNACION PUNTOYCOMA
%token <vname>    ID
%token <vvalue>   NUMBER
%start entorno
%type  <vars>    entorno
%type  <vars>    variables
%type  <var>     variable
%%

entorno:	ABRELLAVE variables CIERRALLAVE { entornolval.vars = $2; }
	;

variables:	variable variables              { $$ = load_variable($1, $2); }
	|       /*empty */                      { $$ = (struct variablesHash*)NULL;  fprintf(stderr, "empty environment\n");}
	;

variable:	ID ASIGNACION NUMBER PUNTOYCOMA  { $$ = create_variable($1, $3); }
	;
%%
