#ifndef CTRLEVALDATA_H
#define CTRLEVALDATA_H


struct Conjunto {
    int id;
};

typedef struct Conjunto    Conjunto_t;
typedef struct Conjunto*   pConjunto_t;
typedef struct LConjuntos  lConjuntos_t;
typedef struct LConjuntos* plConjuntos_t;

struct LConjuntos {
    pConjunto_t pc;
    plConjuntos_t next;
};

char * newCopyString(char*);

pConjunto_t createConjunto(int id);
plConjuntos_t newLConjuntos(void);
plConjuntos_t addListaConjuntos(plConjuntos_t head, pConjunto_t pc);

int lengthLConjuntos(plConjuntos_t list);

#endif
