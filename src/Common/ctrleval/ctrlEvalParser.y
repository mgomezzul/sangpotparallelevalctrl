%{
#include <stdio.h>
#include <stdlib.h>
#include "ctrlEvalData.h"

int yylex(void);
void yyerror (char const *);
%}

%union {
  int  vnumber;
  struct Conjunto* vconjunto;
  struct LConjuntos* vlconjuntos;
  }

%token CONJUNTO ABRELLAVE CIERRALLAVE ABREPARENTESIS CIERRAPARENTESIS PUNTOYCOMA
%token ID
%token <vnumber> NUMBER
%right ASIGNACION
%left SUMA RESTA
%left MULTIPLICACION DIVISION
%left NEG       /* negation --unary minus*/
%start abssyn
%type <vlconjuntos> abssyn
%type <vlconjuntos> evaluadorcfg
%type <vconjunto> conjunto
%type <vnumber> asignaciones
%type <vnumber> asignacion
%type <vnumber> expresion
%%

abssyn:		evaluadorcfg  { yylval.vlconjuntos = $1; }
		              
	;

evaluadorcfg:	conjunto evaluadorcfg  { $$ = addListaConjuntos($2, $1); }
	|	conjunto               { $$ = addListaConjuntos((struct LConjuntos*)NULL, $1); }
	;

conjunto:	CONJUNTO NUMBER ABRELLAVE asignaciones CIERRALLAVE { $$ = createConjunto($2); 
                                                                     printf("creating a set\n");}
        ;

asignaciones:	asignacion asignaciones {}
	|	asignacion  {}
	;

asignacion:	ID ASIGNACION expresion PUNTOYCOMA {}
	;

expresion:	ID  {}
	|	NUMBER { $$ = 0;}
	|	expresion SUMA expresion {}
	|	expresion RESTA expresion {}
	|	expresion MULTIPLICACION expresion {}
	|	expresion DIVISION expresion {}
	|	RESTA expresion %prec NEG {}
	|	ABREPARENTESIS expresion CIERRAPARENTESIS {}
	;

%%
